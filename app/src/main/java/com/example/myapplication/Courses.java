package com.example.myapplication;

public class Courses {

    // variables for storing our data.
    private String routeStart, routeEnd, routeDesc;

    public Courses() {
        // empty constructor
        // required for Firebase.
    }

    // Constructor for all variables.
    public Courses(String routeStart, String routeEnd, String routeDesc) {
        this.routeStart = routeStart;
        this.routeEnd = routeEnd;
        this.routeDesc = routeDesc;
    }

    // getter methods for all variables.
    public String getRouteStart() {
        return routeStart;
    }

    public void setRouteStart(String routeStart) {
        this.routeStart = routeStart;
    }

    public String getRouteEnd() {
        return routeEnd;
    }

    // setter method for all variables.
    public void setRouteEnd(String routeEnd) {
        this.routeEnd = routeEnd;
    }

    public String getRouteDesc() {
        return routeDesc;
    }

    public void setRouteDesc(String routeDesc) {
        this.routeDesc = routeDesc;
    }
}