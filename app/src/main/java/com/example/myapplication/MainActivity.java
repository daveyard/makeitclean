package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {

    // creating variables for our edit text
    private EditText routeStartEdt, routeEndEdt, routeDescEdt;

    // creating variable for button
    private Button submitRouteBtn;

    // creating a strings for storing
    // our values from edittext fields.
    private String routeStart, routeEnd, routeDesc;

    // creating a variable
    // for firebasefirestore.
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // getting our instance
        // from Firebase Firestore.
        db = FirebaseFirestore.getInstance();

        // initializing our edittext and buttons
        routeStartEdt = findViewById(R.id.idEdtRouteStart);
        routeEndEdt = findViewById(R.id.idEdtRouteEnd);
        routeDescEdt = findViewById(R.id.idEdtRouteDescription);
        submitRouteBtn = findViewById(R.id.idBtnSubmitRoute);

        // adding on click listener for button
        submitRouteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getting data from edittext fields.
                routeStart = routeStartEdt.getText().toString();
                routeEnd = routeEndEdt.getText().toString();
                routeDesc = routeDescEdt.getText().toString();

                // validating the text fields if empty or not.
                if (TextUtils.isEmpty(routeStart)) {
                    routeStartEdt.setError("Please enter route start point");
                } else if (TextUtils.isEmpty(routeEnd)) {
                    routeEndEdt.setError("Please enter route end point");
                } else if (TextUtils.isEmpty(routeDesc)) {
                    routeDescEdt.setError("Please enter route description");
                } else {
                    // calling method to add data to Firebase Firestore.
                    addDataToFirestore(routeStart, routeEnd, routeDesc);
                }
            }
        });
    }

    private void addDataToFirestore(String routeStart, String routeEnd, String routeDesc) {

        // creating a collection reference
        // for our Firebase Firestore database.
        CollectionReference dbCourses = db.collection("Courses");

        // adding our data to our courses object class.
        Courses courses = new Courses(routeStart, routeEnd, routeDesc);

        // below method is use to add data to Firebase Firestore.
        dbCourses.add(courses).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                // after the data addition is successful
                // we are displaying a success toast message.
                Toast.makeText(MainActivity.this, "Your Course has been added to Firebase Firestore", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, makeitclean.class);
                startActivity(intent);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // this method is called when the data addition process is failed.
                // displaying a toast message when data addition is failed.
                Toast.makeText(MainActivity.this, "Fail to add route \n" + e, Toast.LENGTH_SHORT).show();
            }
        });
    }
}