package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterClass extends RecyclerView.Adapter<AdapterClass.MyViewHolder> {
    ArrayList<Courses> list;

    public AdapterClass(ArrayList<Courses> list, search search)
    {
        this.list = list;
    }


    @NonNull
    @Override
    public AdapterClass.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_holder,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.start.setText(list.get(i).getRouteStart());
        myViewHolder.end.setText(list.get(i).getRouteEnd());
        myViewHolder.desc.setText(list.get(i).getRouteDesc());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView start,end,desc;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            start = itemView.findViewById(R.id.routeStart);
            end = itemView.findViewById(R.id.routeEnd);
            desc = itemView.findViewById(R.id.description);
        }
    }
}
